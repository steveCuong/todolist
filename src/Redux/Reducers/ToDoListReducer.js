/* eslint-disable import/no-anonymous-default-export */
import {GET_TASK_API} from '../Constants/ToDoListConst';
const initialState = {
    taskList:[]
}

export default (state = initialState, action) => {
    switch (action.type) {

    case GET_TASK_API:
        state.taskList = action.task_list;
        return {...state}

    default:
        return {...state}
    }
}
