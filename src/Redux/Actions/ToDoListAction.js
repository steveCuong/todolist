import Axios from "axios";
import {GET_TASK_API} from '../Constants/ToDoListConst'

export const getTaskListAPI = () => {
    return dispatch => {
        let promise = Axios({
            url: 'http://svcy.myclass.vn/api/ToDoList/GetAllTask',
            method: 'GET'
        });
        promise.then((result) => {
            console.log('abc',result)
            dispatch({
                type:GET_TASK_API,
                task_list: result.data
            })
        })
        promise.catch((err) => {
            console.log('err', err.response.data)
        })
    }
    
}


export const addTaskAPI = (taskNameValue) => {
    return dispatch => {
        let promise = Axios({
            url: 'http://svcy.myclass.vn/api/ToDoList/AddTask',
            method: 'POST',
            data: {taskName:taskNameValue},
        });

        promise.then(result =>{
            alert(result.data + ' is added');
            dispatch(getTaskListAPI());
        });
        promise.catch(err => {
            alert('error')
        })
    }
}

export const deleteTaskAPI = (taskName) => {
    return dispatch => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/deleteTask?taskName=${taskName}`,
            method:'DELETE'
        });
        promise.then((result) => {
            alert(result.data);
            dispatch(getTaskListAPI());
        })
        promise.catch((error) => {
            console.log(error)
        })
    }
}

export const checkDoneAPI = (taskName) => {
    return dispatch => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/doneTask?taskName=${taskName}`,
            method: 'PUT'
        });
        promise.then((result) => {
            alert(result.data);
            dispatch(getTaskListAPI());
        });
        promise.then((error) => {
            alert(error)
        })
    }
}

export const unDoTaskAPI = (taskName) => {
    return dispatch => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/rejectTask?taskName=${taskName}`,
            method: `PUT`
        });
        promise.then((res) => {
            alert(res.data);
            dispatch(getTaskListAPI())
        });
        promise.catch((err) => {
            console.log(err.response.data)
        })
    }
}