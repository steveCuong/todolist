import React, { useState,useEffect } from 'react';
import Axios from 'axios'

export default function TodoListRfc() {

    const [ taskName, setTaskName ] = useState({
        taskList: [],
        values: {
            taskName: '',
        },
        errors: {
            taskName: '',
        }
    });
    const onChange = (event) => {
        let { value, name } = event.target;

        let newValues = { ...taskName };
        newValues = { ...newValues, [name]: value };

        let newError = { ...taskName };
        newError = { ...newError, [name]: value };

        let regrexString = /^[a-z A-Z]+$/;
        if (!regrexString.test(value) || value.trim() === '') {
            newError[name] = name + ' is invalid !';
        } else {
            newError[name] = '';
        }

        setTaskName({
            ...taskName,
            values: newValues,
            errors: newError
        })
    }

    const renderTaskToDo = () => {
        return taskName.taskList.filter(item => !item.status).map((item, index) => {
          return  <li key={index}>
                <span>{item.taskName}</span>
                <div className="buttons">
                    <button className="remove" type="button" onClick={() => {
                        delTask(item.taskName)
                    }}>
                        <i className="fa fa-trash-alt" />
                    </button>
                    <button className="complete" type="button" onClick={() => {checkDone(item.taskName)}}>
                        <i className="far fa-check-circle" />
                        <i className="fas fa-check-circle" />
                    </button>
                </div>
            </li>
        })
    }

    const checkDone = (taskName) => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/doneTask?taskName=${taskName}`,
            method: 'PUT'
        });
        promise.then((result) => {
            alert(result.data);
            getTaskList();
        });
        promise.then((error) => {
            alert(error)
        })
    }

    const getTaskList = () => {
        let promise = Axios({
            url: 'http://svcy.myclass.vn/api/ToDoList/GetAllTask',
            method: 'GET'
        });
        promise.then((result) => {
            console.log('abc')
            setTaskName({
                ...taskName,
                taskList: result.data
            })
        })
        promise.catch((err) => {
            console.log('err', err)
        })
    };

    const renderTaskToDoDone = () => {
        return taskName.taskList.filter(item => item.status).map((item, index) => {
            return <li key={index}>
            <span>{item.taskName}</span>
            <div className="buttons">
                <button className="remove" type="button" onClick={() => {delTask(item.taskName)}}>
                    <i className="fa fa-trash-alt" />
                </button>
                <button className="complete" type="button" onClick={() => {unDoTask(item.taskName)}}>
                    <i className="far fa-undo" />
                    <i className="fas fa-undo" />
                </button>
            </div>
        </li>
        })
    }

    const addTask = (e) => {
        e.preventDefault();
        let promise = Axios({
            url: 'http://svcy.myclass.vn/api/ToDoList/AddTask',
            method: 'POST',
            data: {taskName:taskName.values.taskName},
        });

        promise.then(result =>{
            alert(result.data + ' is added');
            getTaskList();
        });
        promise.catch(err => {
            alert('error')
        })
    }

    const unDoTask = (taskName) => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/rejectTask?taskName=${taskName}`,
            method: `PUT`
        });
        promise.then((res) => {
            alert(res.data);
            getTaskList()
        });
        promise.catch((err) => {
            console.log(err.response.data)
        })
    }

    const delTask = (taskName) => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/deleteTask?taskName=${taskName}`,
            method:'DELETE'
        });
        promise.then((result) => {
            alert(result.data);
            getTaskList();
        })
        promise.catch((error) => {
            console.log(error)
        })
    }

    useEffect(() => {
        getTaskList()
    },[])
    return (
        <form>
            <button>Get task list</button>
            <div className="card">
                <div className="card__header">
                    <img src={require('./bg.png').default} />
                </div>
                {/* <h2>hello!</h2> */}
                <div className="card__body">
                    <div className="card__content">
                        <div className="card__title">
                            <h2>My Tasks</h2>
                            <p>September 9,2020</p>
                        </div>
                        <div className="card__add">
                            <input id="newTask" name="taskName" type="text" placeholder="Enter an activity..." onChange={onChange} />
                            <button id="addItem" type="button" onClick={addTask}>
                                <i className="fa fa-plus" />
                            </button>
                        </div>
                        <p className="text text-danger"></p>
                        <div className="card__todo">
                            {/* Uncompleted tasks */}
                            <ul className="todo" id="todo">
                                {renderTaskToDo()}
                            </ul>
                            {/* Completed tasks */}
                            <ul className="todo" id="completed">
                                {renderTaskToDoDone()}

                                {/* <li>
                                <span>Ăn sáng</span>
                                <div className="buttons">
                                    <button className="remove">
                                        <i className="fa fa-trash-alt" />
                                    </button>
                                    <button className="complete">
                                        <i className="far fa-check-circle" />
                                        <i className="fas fa-check-circle" />
                                    </button>
                                </div>
                            </li> */}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    )
}
