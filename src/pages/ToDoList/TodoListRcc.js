import React, { Component } from 'react';
import Axios from 'axios';
import './TodoList.css'
export default class TodoListRcc extends Component {
    state = {
        taskList: [],
        values:{
            taskName:'',
        },
        errors:{
            taskName:'',
        }
    }
    getTaskList = () => {
        let promise = Axios({
            url: 'http://svcy.myclass.vn/api/ToDoList/GetAllTask',
            method: 'GET'
        });
        promise.then((result) => {
            console.log('abc')
            this.setState({
                ...this.state,
                taskList: result.data
            })
        })
        promise.catch((err) => {
            console.log('err', err)
        })
    };
    renderTaskToDo = () => {
        return this.state.taskList.filter(item => !item.status).map((item, index) => {
          return  <li key={index}>
                <span>{item.taskName}</span>
                <div className="buttons">
                    <button className="remove" type="button" onClick={() => {
                        this.delTask(item.taskName)
                    }}>
                        <i className="fa fa-trash-alt" />
                    </button>
                    <button className="complete" type="button" onClick={() => {this.checkDone(item.taskName)}}>
                        <i className="far fa-check-circle" />
                        <i className="fas fa-check-circle" />
                    </button>
                </div>
            </li>
        })
    }
    
    checkDone = (taskName) => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/doneTask?taskName=${taskName}`,
            method: 'PUT'
        });
        promise.then((result) => {
            alert(result.data);
            this.getTaskList();
        });
        promise.then((error) => {
            alert(error)
        })
    }
    delTask = (taskName) => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/deleteTask?taskName=${taskName}`,
            method:'DELETE'
        });
        promise.then((result) => {
            alert(result.data);
            this.getTaskList();
        })
        promise.catch((error) => {
            console.log(error)
        })
    }

    renderTaskToDoDone = () => {
        return this.state.taskList.filter(item => item.status).map((item, index) => {
            return <li key={index}>
            <span>{item.taskName}</span>
            <div className="buttons">
                <button className="remove" type="button" onClick={() => {this.delTask(item.taskName)}}>
                    <i className="fa fa-trash-alt" />
                </button>
                <button className="complete" type="button" onClick={() => {this.unDoTask(item.taskName)}}>
                    <i className="far fa-undo" />
                    <i className="fas fa-undo" />
                </button>
            </div>
        </li>
        })
    }

    componentDidMount(){
        this.getTaskList();
    }

    onChange = (event) => {
        let {value,name} = event.target;

        let newValues = {...this.state.values};
        newValues = {...newValues,[name]:value};

        let newError = {...this.state.errors};
        newError = {...newError,[name]:value};

        let regrexString = /^[a-z A-Z]+$/;
        if(!regrexString.test(value) || value.trim() === ''){
            newError[name] = name + ' is invalid !';
        }else{
            newError[name] = ''; 
        }

        this.setState({
            ...this.state,
            values: newValues,
            errors: newError
        })
    }

    unDoTask = (taskName) => {
        let promise = Axios({
            url: `http://svcy.myclass.vn/api/ToDoList/rejectTask?taskName=${taskName}`,
            method: `PUT`
        });
        promise.then((res) => {
            alert(res.data);
            this.getTaskList()
        });
        promise.catch((err) => {
            console.log(err.response.data)
        })
    }

    addTask = (e) => {
        e.preventDefault();
        console.log(this.state.values.taskName);
        let promise = Axios({
            url: 'http://svcy.myclass.vn/api/ToDoList/AddTask',
            method: 'POST',
            data: {taskName:this.state.values.taskName},
        });

        promise.then(result =>{
            alert(result.data + ' is added');
            this.getTaskList();
        });
        promise.catch(err => {
            alert('error')
        })
    }
    render() {
        return (
            <form onSubmit={this.addTask}>
                <button onClick={this.getTaskList}>Get task list</button>
                <div className="card">
                    <div className="card__header">
                        <img src={require('./bg.png').default} />
                    </div>
                    {/* <h2>hello!</h2> */}
                    <div className="card__body">
                        <div className="card__content">
                            <div className="card__title">
                                <h2>My Tasks</h2>
                                <p>September 9,2020</p>
                            </div>
                            <div className="card__add">
                                <input id="newTask" name="taskName" type="text" placeholder="Enter an activity..." onChange={this.onChange}/>
                                <button id="addItem" onClick={this.addTask}>
                                    <i className="fa fa-plus" />
                                </button>
                            </div>
                            <p className="text text-danger">{this.state.errors.taskName}</p>
                            <div className="card__todo">
                                {/* Uncompleted tasks */}
                                <ul className="todo" id="todo">
                                    {this.renderTaskToDo()}
                                </ul>
                                {/* Completed tasks */}
                                <ul className="todo" id="completed">
                                    {this.renderTaskToDoDone()}

                                    {/* <li>
                                <span>Ăn sáng</span>
                                <div className="buttons">
                                    <button className="remove">
                                        <i className="fa fa-trash-alt" />
                                    </button>
                                    <button className="complete">
                                        <i className="far fa-check-circle" />
                                        <i className="fas fa-check-circle" />
                                    </button>
                                </div>
                            </li> */}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}
