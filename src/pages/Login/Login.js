import React, { useState } from 'react'

export default function Login(props) {
    const [user, setUser] = useState({userName:'', password:'', status: true});
    const onChange = (event) => {
        const {name,value} = event.target;
        setUser({
            ...user,
            [name]:value
        });
    }

    const handleLogin = (event) => {
        event.preventDefault();
        if(user.userName === 'steven' && user.password === 'steven'){
            localStorage.setItem('userLogin', JSON.stringify(user));
        }else{
            alert('login fail');
            return
        }
    }
    return (
        <form className="container" onSubmit={handleLogin}>
            <div className="form-group">
                <p>User Name</p>
                <input name="userName" className="form-control" onChange={onChange}/>
            </div>
            <div className="form-group">
                <p>Password</p>
                <input name="password" className="form-control" onChange={onChange}/>
            </div>
            <div className="form-group">
                <button className="btn btn-success">Login</button>
            </div>
        </form>
    )
}
