import logo from './logo.svg';
import './App.css';
import Home from './pages/Home/Home';
import About from './pages/About/About';
import Contact from './pages/Contact/Contact';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './Components/Home/Header/Header';
import Login from './pages/Login/Login';
import Detail from './pages/Detail/Detail';
import PageNotFound from './pages/PageNotFound/PageNotFound';
import Profile from './pages/Profile/Profile';
import TodoListRfc from './pages/ToDoList/TodoListRfc';
import TodoListRcc from './pages/ToDoList/TodoListRcc';
import ToDoListRedux from './pages/ToDoList/ToDoListRedux';


function App() {
  return (
    <BrowserRouter>
    <Header/>
      <Switch>
        <Route exact path="/home" component={Home} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/about" component={About} />
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login}/>
        <Route exact path="/profile" component={Profile}/>
        <Route exact path="/rfc" component={TodoListRfc}/>
        <Route exact path="/rcc" component={TodoListRcc}/>
        <Route exact path="/todoListRedux" component={ToDoListRedux}/>
        <Route exact path="/detail/:id" component={Detail}/>
        <Route path="*" component={PageNotFound}/>
      </Switch>
    </BrowserRouter>

  );
}

export default App;
